<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="tr_TR">
<context>
    <name>AboutDialog</name>
    <message>
        <location filename="../../src/windows/about-dialog.ui" line="26"/>
        <source>Dialog</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/windows/about-dialog.ui" line="82"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Noto Sans CJK SC&apos;; font-size:11pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;br /&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/windows/about-dialog.ui" line="109"/>
        <source>TextLabel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/windows/about-dialog.cpp" line="53"/>
        <source>Service &amp; Support: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/windows/about-dialog.cpp" line="94"/>
        <source>Peony is a graphical software to help users manage system files. It provides common file operation functions for users, such as file viewing, file copy, paste, cut, delete, rename, file selection, application opening, file search, file sorting, file preview, etc. it is convenient for users to manage system files intuitively on the interface.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>File Manager</source>
        <translation type="obsolete">Dosya Yöneticisi</translation>
    </message>
    <message>
        <location filename="../../src/windows/about-dialog.cpp" line="69"/>
        <location filename="../../src/windows/about-dialog.cpp" line="83"/>
        <source>Peony</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/windows/about-dialog.cpp" line="87"/>
        <source>Version number: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/windows/about-dialog.cpp" line="118"/>
        <source>none</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>FileLabelBox</name>
    <message>
        <location filename="../../src/control/file-label-box.cpp" line="68"/>
        <source>Rename</source>
        <translation>Yeniden Adlandır</translation>
    </message>
    <message>
        <location filename="../../src/control/file-label-box.cpp" line="73"/>
        <source>Edit Color</source>
        <translation>Rengi Düzenle</translation>
    </message>
    <message>
        <location filename="../../src/control/file-label-box.cpp" line="81"/>
        <source>Delete</source>
        <translation>Sil</translation>
    </message>
    <message>
        <location filename="../../src/control/file-label-box.cpp" line="86"/>
        <source>Create New Label</source>
        <translation>Yeni Etiket Oluştur</translation>
    </message>
</context>
<context>
    <name>HeaderBar</name>
    <message>
        <source>Create Folder</source>
        <translation type="vanished">Klasör Oluştur</translation>
    </message>
    <message>
        <source>Open Terminal</source>
        <translation type="vanished">Uçbirim Aç</translation>
    </message>
    <message>
        <location filename="../../src/control/header-bar.cpp" line="139"/>
        <source>Go Back</source>
        <translation>Geri</translation>
    </message>
    <message>
        <location filename="../../src/control/header-bar.cpp" line="148"/>
        <source>Go Forward</source>
        <translation>İleri</translation>
    </message>
    <message>
        <location filename="../../src/control/header-bar.cpp" line="210"/>
        <source>Search</source>
        <translation>Ara</translation>
    </message>
    <message>
        <location filename="../../src/control/header-bar.cpp" line="216"/>
        <source>View Type</source>
        <translation>Görünüm Türü</translation>
    </message>
    <message>
        <location filename="../../src/control/header-bar.cpp" line="243"/>
        <source>Sort Type</source>
        <translation>Sıralama Türü</translation>
    </message>
    <message>
        <location filename="../../src/control/header-bar.cpp" line="269"/>
        <source>Option</source>
        <translation>Seçenek</translation>
    </message>
    <message>
        <location filename="../../src/control/header-bar.cpp" line="356"/>
        <source>Operate Tips</source>
        <translation>İşlet İpuçları</translation>
    </message>
    <message>
        <location filename="../../src/control/header-bar.cpp" line="357"/>
        <source>Don&apos;t find any terminal, please install at least one terminal!</source>
        <translation>Herhangi bir uçbirim bulunamadı, lütfen en az bir uçbirim kurun!</translation>
    </message>
    <message>
        <source>Minimize</source>
        <translation type="vanished">Küçült</translation>
    </message>
    <message>
        <source>Close</source>
        <translation type="vanished">Kapat</translation>
    </message>
</context>
<context>
    <name>HeaderBarContainer</name>
    <message>
        <location filename="../../src/control/header-bar.cpp" line="680"/>
        <source>Minimize</source>
        <translation>Küçült</translation>
    </message>
    <message>
        <location filename="../../src/control/header-bar.cpp" line="692"/>
        <source>Maximize/Restore</source>
        <translation>Büyüt/Onar</translation>
    </message>
    <message>
        <source>Restore</source>
        <translation type="vanished">Onar</translation>
    </message>
    <message>
        <source>Maximize</source>
        <translation type="vanished">Büyüt</translation>
    </message>
    <message>
        <location filename="../../src/control/header-bar.cpp" line="713"/>
        <source>Close</source>
        <translation>Kapat</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../../src/windows/main-window.cpp" line="731"/>
        <source>File Manager</source>
        <translation>Dosya Yöneticisi</translation>
    </message>
    <message>
        <location filename="../../src/windows/main-window.cpp" line="325"/>
        <source>Undo</source>
        <translation>Geri</translation>
    </message>
    <message>
        <location filename="../../src/windows/main-window.cpp" line="332"/>
        <source>Redo</source>
        <translation>İleri</translation>
    </message>
    <message>
        <location filename="../../src/windows/main-window.cpp" line="729"/>
        <source>Search</source>
        <translation type="unfinished">Ara</translation>
    </message>
    <message>
        <location filename="../../src/windows/main-window.cpp" line="1515"/>
        <source>Tips info</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/windows/main-window.cpp" line="1516"/>
        <source>Trash has no file need to be cleaned.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Delete file Warning</source>
        <translation type="obsolete">Dosya Silme Uyarısı</translation>
    </message>
    <message>
        <location filename="../../src/windows/main-window.cpp" line="1505"/>
        <source>Delete Permanently</source>
        <translation>Kalıcı Olarak Sil</translation>
    </message>
    <message>
        <location filename="../../src/windows/main-window.cpp" line="1506"/>
        <source>Are you sure that you want to delete these files? Once you start a deletion, the files deleting will never be restored again.</source>
        <translation>Bu dosyaları silmek istediğinizden emin misiniz? Bir silme işlemini başlattığınızda, silinen dosyalar bir daha geri yüklenmeyecektir.</translation>
    </message>
    <message>
        <source>Peony Qt</source>
        <translation type="vanished">Peony Qt</translation>
    </message>
    <message>
        <location filename="../../src/windows/main-window.cpp" line="744"/>
        <source>New Folder</source>
        <translation>Yeni Klasör</translation>
    </message>
</context>
<context>
    <name>NavigationSideBar</name>
    <message>
        <source>All tags...</source>
        <translation type="vanished">Tüm etiketler...</translation>
    </message>
    <message>
        <source>Open In &amp;New Window</source>
        <translation type="obsolete">Yeni Pencerede Aç</translation>
    </message>
    <message>
        <location filename="../../src/control/navigation-side-bar.cpp" line="184"/>
        <source>Open In New Window</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/control/navigation-side-bar.cpp" line="200"/>
        <location filename="../../src/control/navigation-side-bar.cpp" line="234"/>
        <source>Can not open %1, %2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/control/navigation-side-bar.cpp" line="218"/>
        <source>Open In New Tab</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Open In New &amp;Tab</source>
        <translation type="obsolete">Yeni Sekmede Aç</translation>
    </message>
</context>
<context>
    <name>NavigationSideBarContainer</name>
    <message>
        <location filename="../../src/control/navigation-side-bar.cpp" line="448"/>
        <source>All tags...</source>
        <translation>Tüm etiketler...</translation>
    </message>
</context>
<context>
    <name>NavigationTabBar</name>
    <message>
        <location filename="../../src/control/navigation-tab-bar.cpp" line="121"/>
        <source>Search &quot;%1&quot; in &quot;%2&quot;</source>
        <translation>&quot;%2&quot; de &quot;%1&quot; ara</translation>
    </message>
</context>
<context>
    <name>OperationMenu</name>
    <message>
        <source>Advance Search</source>
        <translation type="vanished">Gelişmiş Arama</translation>
    </message>
    <message>
        <location filename="../../src/control/operation-menu.cpp" line="68"/>
        <source>Keep Allow</source>
        <translation>İzin Ver</translation>
    </message>
    <message>
        <location filename="../../src/control/operation-menu.cpp" line="77"/>
        <source>Show Hidden</source>
        <translation>Gizlileri Göster</translation>
    </message>
    <message>
        <location filename="../../src/control/operation-menu.cpp" line="84"/>
        <source>Forbid thumbnailing</source>
        <translation>Küçük Resimleri Yasakla</translation>
    </message>
    <message>
        <location filename="../../src/control/operation-menu.cpp" line="93"/>
        <source>Resident in Backend</source>
        <translation>Arka Uçta Yerleşik</translation>
    </message>
    <message>
        <location filename="../../src/control/operation-menu.cpp" line="102"/>
        <source>Parallel Operations</source>
        <translation>Paralel İşlemler</translation>
    </message>
    <message>
        <location filename="../../src/control/operation-menu.cpp" line="111"/>
        <source>Help</source>
        <translation>Yardım</translation>
    </message>
    <message>
        <location filename="../../src/control/operation-menu.cpp" line="115"/>
        <source>About</source>
        <translation>Hakkında</translation>
    </message>
</context>
<context>
    <name>OperationMenuEditWidget</name>
    <message>
        <location filename="../../src/control/operation-menu.cpp" line="146"/>
        <source>Edit</source>
        <translation>Düzenle</translation>
    </message>
    <message>
        <location filename="../../src/control/operation-menu.cpp" line="157"/>
        <source>copy</source>
        <translation type="unfinished">Kopyala</translation>
    </message>
    <message>
        <location filename="../../src/control/operation-menu.cpp" line="166"/>
        <source>paste</source>
        <translation type="unfinished">Yapıştır</translation>
    </message>
    <message>
        <location filename="../../src/control/operation-menu.cpp" line="175"/>
        <source>cut</source>
        <translation type="unfinished">Kes</translation>
    </message>
    <message>
        <location filename="../../src/control/operation-menu.cpp" line="184"/>
        <source>trash</source>
        <translation type="unfinished">Çöp</translation>
    </message>
</context>
<context>
    <name>PeonyApplication</name>
    <message>
        <source>Peony-Qt</source>
        <translation type="vanished">Dosya Yöneticisi</translation>
    </message>
    <message>
        <location filename="../../src/peony-application.cpp" line="141"/>
        <source>peony-qt</source>
        <translation>Dosya Yöneticisi</translation>
    </message>
    <message>
        <location filename="../../src/peony-application.cpp" line="148"/>
        <source>Files or directories to open</source>
        <translation>Açılacak dosyalar veya dizinler</translation>
    </message>
    <message>
        <location filename="../../src/peony-application.cpp" line="148"/>
        <source>[FILE1, FILE2,...]</source>
        <translation>[DOSYA1, DOSYA2,...]</translation>
    </message>
    <message>
        <location filename="../../src/peony-application.cpp" line="188"/>
        <source>Warning</source>
        <translation>Uyarı</translation>
    </message>
    <message>
        <location filename="../../src/peony-application.cpp" line="188"/>
        <source>Peony-Qt can not get the system&apos;s icon theme. There are 2 reasons might lead to this problem:

1. Peony-Qt might be running as root, that means you have the higher permission and can do some things which normally forbidden. But, you should learn that if you were in a root, the virtual file system will lose some featrue such as you can not use &quot;My Computer&quot;, the theme and icons might also went wrong. So, run peony-qt in a root is not recommended.

2. You are using a non-qt theme for your system but you didn&apos;t install the platform theme plugin for qt&apos;s applications. If you are using gtk-theme, try installing the qt5-gtk2-platformtheme package to resolve this problem.</source>
        <translation>Peony-Qt sistem simgesi teması alınamıyor. Bu soruna yol açabilecek 2 neden vardır:

1. Peony-Qt root olarak çalışıyor olabilir, bu da daha yüksek izne sahip olduğunuz ve normalde yasak olan bazı şeyleri yapabileceğiniz anlamına gelir. Ancak, eğer root iseniz, sanal dosya sisteminin Bilgisayarımı kullanamayacağınız gibi bazı özellikleri kaybedeceğini, temanın ve simgelerin de yanlış gidebileceğini öğrenmelisiniz. Yani, bir root olarak Peony-qt çalıştırmak önerilmez.

2. Sisteminiz için qt olmayan bir tema kullanıyorsunuz ancak qt uygulamaları için platform tema eklentisini yüklemediniz. Gtk-theme kullanıyorsanız, bu sorunu çözmek için qt5-gtk2-platformtheme paketini yüklemeyi deneyin.</translation>
    </message>
    <message>
        <location filename="../../src/peony-application.cpp" line="448"/>
        <source>Peony Qt</source>
        <translation>Dosya Yöneticisi</translation>
    </message>
    <message>
        <location filename="../../src/peony-application.cpp" line="449"/>
        <source>Author:
	Yue Lan &lt;lanyue@kylinos.cn&gt;
	Meihong He &lt;hemeihong@kylinos.cn&gt;

Copyright (C): 2020, KylinSoft Co., Ltd.</source>
        <translation type="unfinished">Yazar:
	Yue Lan &lt;lanyue@kylinos.cn&gt;
	Meihong He &lt;hemeihong@kylinos.cn&gt;

Telif Hakkı (C): 2020, KylinSoft Co., Ltd.</translation>
    </message>
    <message>
        <source>Author:
	Yue Lan &lt;lanyue@kylinos.cn&gt;
	Meihong He &lt;hemeihong@kylinos.cn&gt;

Copyright (C): 2019-2020, KYLIN Software Co., Ltd.</source>
        <translation type="vanished">Yazar:
	Yue Lan &lt;lanyue@kylinos.cn&gt;
	Meihong He &lt;hemeihong@kylinos.cn&gt;

Telif Hakkı(C): 2019-2020, KYLIN Software Co., Ltd.</translation>
    </message>
    <message>
        <source>Author: 
	Yue Lan &lt;lanyue@kylinos.cn&gt;
	Meihong He &lt;hemeihong@kylinos.cn&gt;

Copyright (C): 2019-2020, KYLIN Software Co., Ltd.</source>
        <translation type="vanished">Yazar:
	Yue Lan &lt;lanyue@kylinos.cn&gt;
	Meihong He &lt;hemeihong@kylinos.cn&gt;

Telif (C): 2019-2020, KYLIN Software Co., Ltd.</translation>
    </message>
    <message>
        <source>Authour: 
	Yue Lan &lt;lanyue@kylinos.cn&gt;
	Meihong He &lt;hemeihong@kylinos.cn&gt;

Copyright (C): 2019-2020, KYLIN Software Co., Ltd.</source>
        <translation type="vanished">Yazar:
	Yue Lan &lt;lanyue@kylinos.cn&gt;
	Meihong He &lt;hemeihong@kylinos.cn&gt;

Telif (C): 2019-2020, KYLIN Software Co., Ltd.</translation>
    </message>
    <message>
        <source>Authour: 
	Yue Lan &lt;lanyue@kylinos.cn&gt;
	Meihong He &lt;hemeihong@kylinos.cn&gt;

Copyright (C): 2019-2020, Tianjin KYLIN Information Technology Co., Ltd.</source>
        <translation type="vanished">Yazar:
	Yue Lan &lt;lanyue@kylinos.cn&gt;
	Meihong He &lt;hemeihong@kylinos.cn&gt;

Telif (C): 2019-2020, Tianjin KYLIN Information Technology Co., Ltd.</translation>
    </message>
    <message>
        <location filename="../../src/peony-application.h" line="67"/>
        <source>Close all peony-qt windows and quit</source>
        <translation>Tüm peony-qt pencerelerini kapatın ve çıkın</translation>
    </message>
    <message>
        <location filename="../../src/peony-application.h" line="68"/>
        <source>Show items</source>
        <translation>Öğeleri göster</translation>
    </message>
    <message>
        <location filename="../../src/peony-application.h" line="69"/>
        <source>Show folders</source>
        <translation>Klasörleri göster</translation>
    </message>
    <message>
        <location filename="../../src/peony-application.h" line="70"/>
        <source>Show properties</source>
        <translation>Seçenekleri göster</translation>
    </message>
</context>
<context>
    <name>SortTypeMenu</name>
    <message>
        <location filename="../../src/control/sort-type-menu.cpp" line="33"/>
        <source>File Name</source>
        <translation>Dosya Adı</translation>
    </message>
    <message>
        <location filename="../../src/control/sort-type-menu.cpp" line="45"/>
        <source>File Size</source>
        <translation>Dosya Boyutu</translation>
    </message>
    <message>
        <location filename="../../src/control/sort-type-menu.cpp" line="41"/>
        <source>File Type</source>
        <translation>Dosya Türü</translation>
    </message>
    <message>
        <location filename="../../src/control/sort-type-menu.cpp" line="37"/>
        <source>Modified Date</source>
        <translation>Düzenleme Tarihi</translation>
    </message>
    <message>
        <source>Modified Data</source>
        <translation type="vanished">Düzenlenen Veri</translation>
    </message>
    <message>
        <location filename="../../src/control/sort-type-menu.cpp" line="60"/>
        <source>Ascending</source>
        <translation>Artan</translation>
    </message>
    <message>
        <location filename="../../src/control/sort-type-menu.cpp" line="64"/>
        <source>Descending</source>
        <translation>Azalan</translation>
    </message>
</context>
<context>
    <name>TabStatusBar</name>
    <message>
        <source>; %1 folders</source>
        <translation type="vanished">; %1 klasör</translation>
    </message>
    <message>
        <source>; %1 files, %2 total</source>
        <translation type="vanished">; %1 dosya, %2 toplam</translation>
    </message>
    <message>
        <source>; %1 folder</source>
        <translation type="vanished">; %1 klasör</translation>
    </message>
    <message>
        <source>; %1 file, %2</source>
        <translation type="vanished">; %1 dosya, %2</translation>
    </message>
    <message>
        <source>%1 selected</source>
        <translation type="vanished">%1 seçildi</translation>
    </message>
    <message>
        <source>Search &quot;%1&quot; in &quot;%2&quot;</source>
        <translation type="vanished">%1 de %1 bul</translation>
    </message>
    <message>
        <location filename="../../src/control/tab-status-bar.cpp" line="102"/>
        <source> %1 items </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/control/tab-status-bar.cpp" line="135"/>
        <source> selected %1 items    %2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/control/tab-status-bar.cpp" line="137"/>
        <source> selected %1 items</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>TabWidget</name>
    <message>
        <location filename="../../src/control/tab-widget.cpp" line="196"/>
        <source>Trash</source>
        <translation>Çöp</translation>
    </message>
    <message>
        <location filename="../../src/control/tab-widget.cpp" line="200"/>
        <source>Clear</source>
        <translation>Temizle</translation>
    </message>
    <message>
        <location filename="../../src/control/tab-widget.cpp" line="204"/>
        <source>Recover</source>
        <translation>Geri Yükle</translation>
    </message>
    <message>
        <location filename="../../src/control/tab-widget.cpp" line="296"/>
        <source>Close Filter.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/control/tab-widget.cpp" line="308"/>
        <source>Filter</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Close advance search.</source>
        <translation type="vanished">Gelişmiş aramayı kapat.</translation>
    </message>
    <message>
        <source>Search</source>
        <translation type="vanished">Ara</translation>
    </message>
    <message>
        <location filename="../../src/control/tab-widget.cpp" line="317"/>
        <source>Choose other path to search.</source>
        <translation>Aramak için başka bir yol seçin.</translation>
    </message>
    <message>
        <location filename="../../src/control/tab-widget.cpp" line="326"/>
        <source>Search recursively</source>
        <translation>Yinelemeli olarak ara</translation>
    </message>
    <message>
        <source>more options</source>
        <translation type="vanished">Daha fazla seçenek</translation>
    </message>
    <message>
        <source>Show/hide advance search</source>
        <translation type="vanished">Gelişmiş aramayı göster/gizle</translation>
    </message>
    <message>
        <location filename="../../src/control/tab-widget.cpp" line="400"/>
        <source>Select path</source>
        <translation type="unfinished">Yol seç</translation>
    </message>
    <message>
        <location filename="../../src/control/tab-widget.cpp" line="435"/>
        <location filename="../../src/control/tab-widget.cpp" line="532"/>
        <source>is</source>
        <translation>de</translation>
    </message>
    <message>
        <location filename="../../src/control/tab-widget.cpp" line="453"/>
        <source>Please input key words...</source>
        <translation>Lütfen anahtar kelimeler girin ...</translation>
    </message>
    <message>
        <source>Please input kay words...</source>
        <translation type="vanished">Lütfen anahtar kelimeler girin ...</translation>
    </message>
    <message>
        <location filename="../../src/control/tab-widget.cpp" line="498"/>
        <location filename="../../src/control/tab-widget.cpp" line="519"/>
        <source>contains</source>
        <translation>İçerik</translation>
    </message>
    <message>
        <location filename="../../src/control/tab-widget.h" line="260"/>
        <source>name</source>
        <translation>İsim</translation>
    </message>
    <message>
        <location filename="../../src/control/tab-widget.h" line="260"/>
        <source>type</source>
        <translation>Tür</translation>
    </message>
    <message>
        <location filename="../../src/control/tab-widget.h" line="260"/>
        <source>modify time</source>
        <translation>Değiştirme Zamanı</translation>
    </message>
    <message>
        <location filename="../../src/control/tab-widget.h" line="260"/>
        <source>file size</source>
        <translation>Dosya Boyutu</translation>
    </message>
    <message>
        <location filename="../../src/control/tab-widget.h" line="261"/>
        <location filename="../../src/control/tab-widget.h" line="263"/>
        <location filename="../../src/control/tab-widget.h" line="264"/>
        <source>all</source>
        <translation>Tümü</translation>
    </message>
    <message>
        <location filename="../../src/control/tab-widget.h" line="261"/>
        <source>file folder</source>
        <translation>Dosya klasör</translation>
    </message>
    <message>
        <location filename="../../src/control/tab-widget.h" line="261"/>
        <source>image</source>
        <translation>Resim</translation>
    </message>
    <message>
        <location filename="../../src/control/tab-widget.h" line="261"/>
        <source>video</source>
        <translation>Video</translation>
    </message>
    <message>
        <location filename="../../src/control/tab-widget.h" line="262"/>
        <source>text file</source>
        <translation>Metin dosyası</translation>
    </message>
    <message>
        <location filename="../../src/control/tab-widget.h" line="262"/>
        <source>audio</source>
        <translation>Ses</translation>
    </message>
    <message>
        <location filename="../../src/control/tab-widget.h" line="262"/>
        <source>others</source>
        <translation>Diğer</translation>
    </message>
    <message>
        <location filename="../../src/control/tab-widget.h" line="262"/>
        <source>wps file</source>
        <translation type="unfinished">Wps Dosyası</translation>
    </message>
    <message>
        <location filename="../../src/control/tab-widget.h" line="263"/>
        <source>today</source>
        <translation>Bugün</translation>
    </message>
    <message>
        <location filename="../../src/control/tab-widget.h" line="263"/>
        <source>this week</source>
        <translation>Bu hafta</translation>
    </message>
    <message>
        <location filename="../../src/control/tab-widget.h" line="263"/>
        <source>this month</source>
        <translation>Bu ay</translation>
    </message>
    <message>
        <location filename="../../src/control/tab-widget.h" line="263"/>
        <source>this year</source>
        <translation>Bu yıl</translation>
    </message>
    <message>
        <location filename="../../src/control/tab-widget.h" line="263"/>
        <source>year ago</source>
        <translation>Geçen yıl</translation>
    </message>
    <message>
        <location filename="../../src/control/tab-widget.h" line="264"/>
        <source>tiny(0-16K)</source>
        <translation>Çok küçük(0-16K)</translation>
    </message>
    <message>
        <location filename="../../src/control/tab-widget.h" line="264"/>
        <source>small(16k-1M)</source>
        <translation>Küçük(16k-1M)</translation>
    </message>
    <message>
        <location filename="../../src/control/tab-widget.h" line="264"/>
        <source>empty(0K)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/control/tab-widget.h" line="264"/>
        <source>medium(1M-128M)</source>
        <translation type="unfinished">Orta(1M-100M) {1M?} {128M?}</translation>
    </message>
    <message>
        <location filename="../../src/control/tab-widget.h" line="264"/>
        <source>big(128M-1G)</source>
        <translation type="unfinished">Büyük(100M-1G) {128M?} {1G?}</translation>
    </message>
    <message>
        <location filename="../../src/control/tab-widget.h" line="264"/>
        <source>large(1-4G)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/control/tab-widget.h" line="264"/>
        <source>great(&gt;4G)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>medium(1M-100M)</source>
        <translation type="vanished">Orta(1M-100M)</translation>
    </message>
    <message>
        <source>big(100M-1G)</source>
        <translation type="vanished">Büyük(100M-1G)</translation>
    </message>
    <message>
        <source>large(&gt;1G)</source>
        <translation type="vanished">Daha büyük(&gt;1G)</translation>
    </message>
</context>
</TS>

<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="cs">
<context>
    <name>Peony::DesktopIconView</name>
    <message>
        <location filename="../../peony-qt-desktop/desktop-icon-view.h" line="81"/>
        <source>Desktop Icon View</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../peony-qt-desktop/desktop-icon-view.cpp" line="636"/>
        <source>New Folder</source>
        <translation type="unfinished">Yeni Klasör</translation>
    </message>
    <message>
        <location filename="../../peony-qt-desktop/desktop-icon-view.cpp" line="729"/>
        <source>set background</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../peony-qt-desktop/desktop-icon-view.cpp" line="909"/>
        <source>Open Link failed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../peony-qt-desktop/desktop-icon-view.cpp" line="910"/>
        <source>File not exist, do you want to delete the link file?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../peony-qt-desktop/desktop-icon-view.cpp" line="925"/>
        <source>Open failed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../peony-qt-desktop/desktop-icon-view.cpp" line="926"/>
        <source>Open directory failed, you have no permission!</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Peony::DesktopMenu</name>
    <message>
        <location filename="../../peony-qt-desktop/desktop-menu.cpp" line="235"/>
        <source>Reverse Select</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&amp;New...</source>
        <translation type="vanished">&amp;Yeni...</translation>
    </message>
    <message>
        <location filename="../../peony-qt-desktop/desktop-menu.cpp" line="249"/>
        <source>New...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../peony-qt-desktop/desktop-menu.cpp" line="330"/>
        <source>New Folder</source>
        <translation type="unfinished">Yeni Klasör</translation>
    </message>
    <message>
        <location filename="../../peony-qt-desktop/desktop-menu.cpp" line="347"/>
        <source>View Type...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../peony-qt-desktop/desktop-menu.cpp" line="398"/>
        <source>Sort By...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../peony-qt-desktop/desktop-menu.cpp" line="403"/>
        <source>Name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../peony-qt-desktop/desktop-menu.cpp" line="405"/>
        <source>File Type</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../peony-qt-desktop/desktop-menu.cpp" line="406"/>
        <source>File Size</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../peony-qt-desktop/desktop-menu.cpp" line="404"/>
        <source>Modified Date</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../peony-qt-desktop/desktop-menu.cpp" line="109"/>
        <source>Open in new Window</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../peony-qt-desktop/desktop-menu.cpp" line="114"/>
        <source>Select All</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../peony-qt-desktop/desktop-menu.cpp" line="137"/>
        <location filename="../../peony-qt-desktop/desktop-menu.cpp" line="165"/>
        <location filename="../../peony-qt-desktop/desktop-menu.cpp" line="197"/>
        <location filename="../../peony-qt-desktop/desktop-menu.cpp" line="208"/>
        <source>Open</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../peony-qt-desktop/desktop-menu.cpp" line="145"/>
        <location filename="../../peony-qt-desktop/desktop-menu.cpp" line="170"/>
        <source>Open with...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../peony-qt-desktop/desktop-menu.cpp" line="158"/>
        <location filename="../../peony-qt-desktop/desktop-menu.cpp" line="191"/>
        <source>More applications...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../peony-qt-desktop/desktop-menu.cpp" line="211"/>
        <source>Open %1 selected files</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../peony-qt-desktop/desktop-menu.cpp" line="316"/>
        <source>Empty File</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../peony-qt-desktop/desktop-menu.cpp" line="326"/>
        <source>Folder</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../peony-qt-desktop/desktop-menu.cpp" line="353"/>
        <source>Small</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../peony-qt-desktop/desktop-menu.cpp" line="358"/>
        <source>Normal</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../peony-qt-desktop/desktop-menu.cpp" line="363"/>
        <source>Large</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../peony-qt-desktop/desktop-menu.cpp" line="368"/>
        <source>Huge</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../peony-qt-desktop/desktop-menu.cpp" line="478"/>
        <source>Clean the trash</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../peony-qt-desktop/desktop-menu.cpp" line="492"/>
        <source>Copy</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../peony-qt-desktop/desktop-menu.cpp" line="496"/>
        <source>Cut</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../peony-qt-desktop/desktop-menu.cpp" line="517"/>
        <source>Delete to trash</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../peony-qt-desktop/desktop-menu.cpp" line="526"/>
        <source>Delete forever</source>
        <translation>Kalıcı olarak sil</translation>
    </message>
    <message>
        <location filename="../../peony-qt-desktop/desktop-menu.cpp" line="534"/>
        <source>Rename</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../peony-qt-desktop/desktop-menu.cpp" line="541"/>
        <source>Paste</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../peony-qt-desktop/desktop-menu.cpp" line="547"/>
        <source>Refresh</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../peony-qt-desktop/desktop-menu.cpp" line="561"/>
        <source>Properties</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>P&amp;roperties</source>
        <translation type="obsolete">Özellikler</translation>
    </message>
    <message>
        <source>Sort Order...</source>
        <translation type="vanished">Sıralama Düzeni...</translation>
    </message>
    <message>
        <source>Ascending Order</source>
        <translation type="vanished">Artan Düzen</translation>
    </message>
    <message>
        <source>Descending Order</source>
        <translation type="vanished">Azalan Düzen</translation>
    </message>
    <message>
        <source>Zoom &amp;In</source>
        <translation type="vanished">Yakınlaştır</translation>
    </message>
    <message>
        <source>Zoom &amp;Out</source>
        <translation type="vanished">Uzaklaştır</translation>
    </message>
    <message>
        <location filename="../../peony-qt-desktop/desktop-menu.cpp" line="480"/>
        <source>Delete Permanently</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../peony-qt-desktop/desktop-menu.cpp" line="480"/>
        <source>Are you sure that you want to delete these files? Once you start a deletion, the files deleting will never be restored again.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Cu&amp;t</source>
        <translation type="vanished">Kes</translation>
    </message>
    <message>
        <source>&amp;Delete</source>
        <translation type="vanished">Sil</translation>
    </message>
    <message>
        <source>&amp;Rename</source>
        <translation type="vanished">Yeniden Adlandır</translation>
    </message>
    <message>
        <source>&amp;Properties</source>
        <translation type="vanished">Özellikler</translation>
    </message>
</context>
<context>
    <name>Peony::DesktopWindow</name>
    <message>
        <location filename="../../peony-qt-desktop/desktop-window.cpp" line="96"/>
        <source>Desktop</source>
        <translation type="unfinished">Masaüstü</translation>
    </message>
    <message>
        <source>New Folder</source>
        <translation type="vanished">Yeni Klasör</translation>
    </message>
</context>
<context>
    <name>PeonyDesktopApplication</name>
    <message>
        <source>Close the peony-qt desktop window</source>
        <translation type="vanished">Masaüstünü kapatın ve çıkın</translation>
    </message>
    <message>
        <location filename="../../peony-qt-desktop/peony-desktop-application.cpp" line="151"/>
        <source>peony-qt-desktop</source>
        <translation>Masaüstü</translation>
    </message>
    <message>
        <source>Peony-Qt Desktop</source>
        <translation type="vanished">Masaüstü</translation>
    </message>
    <message>
        <source>Desktop</source>
        <translation type="vanished">Masaüstü</translation>
    </message>
    <message>
        <location filename="../../peony-qt-desktop/peony-desktop-application.cpp" line="254"/>
        <source>Close the peony desktop window</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../peony-qt-desktop/peony-desktop-application.cpp" line="257"/>
        <source>Take over the dbus service.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../peony-qt-desktop/peony-desktop-application.cpp" line="260"/>
        <source>Take over the desktop displaying</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../peony-qt-desktop/peony-desktop-application.cpp" line="351"/>
        <source>set background</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
